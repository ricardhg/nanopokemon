

export default class Team {

    constructor (id=0, nombre='', pokemon1='', pokemon2='', pokemon3='', pokemon4='') {
       this.id = id;
       this.nombre = nombre;
       this.pokemon1 = pokemon1;
       this.pokemon2 = pokemon2;
       this.pokemon3 = pokemon3;
       this.pokemon4 = pokemon4;
    }

}