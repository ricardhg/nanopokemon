import React from "react";
import { BrowserRouter, Link, NavLink, Switch, Route } from "react-router-dom";
import { Container, Row, Col, Button } from 'reactstrap';

// importem components
import Inici from './components/Inici';
import NavMenu from './components/NavMenu';
import P404 from './components/P404';
import ListaPokemons from './components/ListaPokemons';
import DetallPokemon from './components/DetallPokemon';
import Teams from './components/Teams';
import TeamEdit from './components/TeamEdit';

//importem models
import Pokemon from './models/Pokemon';


// importamos css
import 'bootstrap/dist/css/bootstrap.min.css';
import 'font-awesome/css/font-awesome.min.css';
import './app.css';


//xmysql -h localhost -u root -p admin -d codesplai

// clase App 
export default class App extends React.Component {


  render() {


    return (
      <BrowserRouter>
        <NavMenu />
        <Container>
          <Row>
            <Col>
              <Switch>
                <Route exact path="/" component={Inici} />
                <Route exact path="/pokemons" component={ListaPokemons} />
                <Route exact path="/pokemons/:id" component={DetallPokemon} />
                <Route exact path="/teams" component={Teams} />
                <Route exact path="/team-edit/:id?" component={TeamEdit} />
                <Route component={P404} />
              </Switch>
            </Col>
          </Row>
        </Container>
      </BrowserRouter>
    );

  }

}
