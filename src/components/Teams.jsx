import React from "react";

import { Button, Table } from 'reactstrap';
import { Redirect, Link } from 'react-router-dom';

const  IconoPokemon = (props) => <img src={"https://img.pokemondb.net/sprites/omega-ruby-alpha-sapphire/dex/normal/"+props.nombre.toLowerCase()+".png"} />;
const  FotoPokemon = (props) => <img width="100px" src={"https://img.pokemondb.net/artwork/large/"+props.nombre.toLowerCase()+".jpg"} />;

export default class ListaPokemons extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            teams: [],
        }
        this.getData = this.getData.bind(this);
        
        this.getData();
    }


    //responsable de "disparar" el nuevo fetch cuando
    //volvemos de editar, para actualizar la lista
    componentDidUpdate(){
        this.getData();
    }

    getData() {
        const fetchURL = "http://localhost:3000/api/teams";
        fetch(fetchURL)
            .then(results => results.json())
            .then(data => this.setState({ teams: data }))
            .catch(err => console.log(err));
    }


    render() {

        if (this.state.teams.length === 0) {
            return <>...</>;
        }


        //creem les files a partir de dades rebudes, ordenades per id
        let filas = this.state.teams.map(item => {
            return (
                <tr key={item.id}>
                    <td>{item.id}</td>
                    <td>{item.nombre}</td>
                    <td><IconoPokemon nombre={item.pokemon1} /></td>
                    <td><IconoPokemon nombre={item.pokemon2} /></td>
                    <td><IconoPokemon nombre={item.pokemon3} /></td>
                    <td><IconoPokemon nombre={item.pokemon4} /></td>
                    <td><Link to={"/team-edit/"+item.id} className="btn btn-primary" >Edit</Link></td>
                </tr>
            );
        })

        return (
            <>
                <h1 className="titol-vista">Pokemons</h1>
                <Table>
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Nombre</th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        {filas}
                    </tbody>
                </Table>
                <Link className="btn btn-primary" to="/team-edit" >Nou Equip</Link>
            </>
        );
    }

}

