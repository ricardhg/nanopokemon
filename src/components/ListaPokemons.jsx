import React from "react";

import { Button, ButtonGroup, Table } from 'reactstrap';
import { Redirect, Link } from 'react-router-dom';

const  IconoPokemon = (props) => <img src={"https://img.pokemondb.net/sprites/omega-ruby-alpha-sapphire/dex/normal/"+props.nombre.toLowerCase()+".png"} />;
const  FotoPokemon = (props) => <img width="100px" src={"https://img.pokemondb.net/artwork/large/"+props.nombre.toLowerCase()+".jpg"} />;

export default class ListaPokemons extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            pokemons: [],
            page: 1
        }
        this.getData = this.getData.bind(this);
        this.pgDown = this.pgDown.bind(this);
        this.pgUp = this.pgUp.bind(this);
        this.getData();
    }

    getData() {
        const fetchURL = "http://localhost:3000/api/pokemons?_p="+this.state.page+"&_size=25";
        fetch(fetchURL)
            .then(results => results.json())
            .then(data => this.setState({ pokemons: data }))
            .catch(err => console.log(err));
    }

    pgDown(){
        this.setState({page: this.state.page - 1}, this.getData);
    }

    pgUp(){
        this.setState({page: this.state.page + 1}, this.getData);
    }


    render() {

        if (this.state.pokemons.length === 0) {
            return <>...</>;
        }


        //creem les files a partir de dades rebudes, ordenades per id
        let filas = this.state.pokemons.map(item => {
            return (
                <tr key={item.id}>
                    <td>{item.id}</td>
                    <td><img src={"https://img.pokemondb.net/sprites/omega-ruby-alpha-sapphire/dex/normal/"+item.nombre.toLowerCase()+".png"} /></td>
                    <td>{item.nombre}</td>
                    <td>{item.caracter}</td>
                    <td><Link to={"/pokemons/"+item.id} >Detall</Link></td>
             
                </tr>
            );
        })

        return (
            <>
                <h1 className="titol-vista">Pokemons</h1>
                <ButtonGroup>
                    <Button color="primary"onClick={this.pgDown}>{'Prev'}</Button>
                    <Button color="secondary">{"pg. "+this.state.page}</Button>
                    <Button color="primary"onClick={this.pgUp}>{'Next'}</Button>
                </ButtonGroup>  
                <br />
                <br />
                <Table>
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Foto</th>
                            <th>Nombre</th>
                            <th>Caracter</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        {filas}
                    </tbody>
                </Table>

                <Button onClick={this.pgDown}>{'Prev'}</Button>
                <Button onClick={this.pgUp}>{'Next'}</Button>
            </>
        );
    }

}

