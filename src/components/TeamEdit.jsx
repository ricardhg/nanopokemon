
import React, { Component } from 'react';
import { Redirect } from 'react-router-dom';

import { Button, Form } from 'reactstrap';
import { Row, Col, FormGroup, Input, Label, Select } from 'reactstrap';

import Team from '../models/Team'

class TeamEdit extends Component {
    constructor(props) {
        super(props);

        let id = 0;
        if (this.props.match && this.props.match.params.id) {
            id = this.props.match.params.id * 1;
        }

        this.state = {
            id: id,
            nombre: '',
            pokemon1: '',
            pokemon2: '',
            pokemon3: '',
            pokemon4: '',
            pokemons: [],
            volver: false,
        }


        this.cargaTeam = this.cargaTeam.bind(this);
        this.cargaPokemons = this.cargaPokemons.bind(this);

        this.cambioInput = this.cambioInput.bind(this);
        this.submit = this.submit.bind(this);

        if (id > 0) {
            this.cargaTeam(id);
        }
        this.cargaPokemons();
    }

    cargaTeam(id) {
        const fetchURL = "http://localhost:3000/api/teams/" + id;
        fetch(fetchURL)
            .then(results => results.json())
            .then(data => {
                let team = data[0];
                this.setState({
                    nombre: team.nombre,
                    pokemon1: team.pokemon1,
                    pokemon2: team.pokemon2,
                    pokemon3: team.pokemon3,
                    pokemon4: team.pokemon4,
                });
            })
            .catch(err => console.log(err));
    }

    cargaPokemons() {
        const fetchURL = "http://localhost:3000/api/pokemons?_size=1000";
        fetch(fetchURL)
            .then(results => results.json())
            .then(data => this.setState({ pokemons: data }))
            .catch(err => console.log(err));
    }


    cambioInput(event) {
        const value = event.target.type === 'checkbox' ? event.target.checked : event.target.value;
        const name = event.target.name;
        this.setState({ [name]: value });
    }

    submit(e) {
        e.preventDefault();

        let team = {
            nombre: this.state.nombre,
            pokemon1: this.state.pokemon1,
            pokemon2: this.state.pokemon2,
            pokemon3: this.state.pokemon3,
            pokemon4: this.state.pokemon4,
        };

        if (this.state.id > 0) {
            team.id = this.state.id;
        }

        let teamJSON = JSON.stringify(team);
        let method = (this.state.id > 0) ? 'PUT' : 'POST';
        fetch("http://localhost:3000/api/teams", {
            method: method,
            headers: new Headers({ 'Content-Type': 'application/json' }),
            body: teamJSON
        })
            .then(resp => resp.json())
            .then((resp) => console.log(resp))
            .catch(err => console.log(err));

        this.setState({ volver: true });

    }


    render() {

        if(this.state.pokemons.length===0){
            return <></>;
        }

        if (this.state.volver) {
            return <Redirect to={'/teams'} />
        }

        let opciones = this.state.pokemons.map(el => <option key={el.nombre} value={el.nombre}>{el.nombre}</option>);

        return (
            <>
                <h1 className="titol-vista">Team </h1>
                <Form onSubmit={this.submit}>
                    <Row>

                        <Col xs="8"  >
                            <FormGroup >
                                <Label for="nombre">Nombre</Label>
                                <Input type="text"
                                    name="nombre"
                                    id="nombre"
                                    value={this.state.nombre}
                                    onChange={this.cambioInput} />
                            </FormGroup>
                        </Col>

                        <Col xs="6"  >
                            <FormGroup >
                                <Label for="pokemon1">Pokemon 1</Label>
                                <Input type="select" 
                                    name="pokemon1"
                                    id="pokemon1"
                                    value={this.state.pokemon1}
                                    onChange={this.cambioInput} >
                                    {opciones}
                                    </Input>
                            </FormGroup>
                        </Col>

                        <Col xs="6"  >
                            <FormGroup >
                                <Label for="pokemon2">Pokemon 2</Label>
                                <Input type="select" 
                                    name="pokemon2"
                                    id="pokemon2"
                                    value={this.state.pokemon2}
                                    onChange={this.cambioInput} >
                                    {opciones}
                                    </Input>
                            </FormGroup>
                        </Col>

                        <Col xs="6"  >
                            <FormGroup >
                                <Label for="pokemon3">Pokemon 3</Label>
                                <Input type="select" 
                                    name="pokemon3"
                                    id="pokemon3"
                                    value={this.state.pokemon3}
                                    onChange={this.cambioInput} >
                                    {opciones}
                                    </Input>
                            </FormGroup>
                        </Col>

                        <Col xs="6"  >
                            <FormGroup >
                                <Label for="pokemon4">Pokemon 4</Label>
                                <Input type="select" 
                                    name="pokemon4"
                                    id="pokemon4"
                                    value={this.state.pokemon4}
                                    onChange={this.cambioInput} >
                                    {opciones}
                                    </Input>
                            </FormGroup>
                        </Col>

                        

                    </Row>
                    <Row>
                        <Col>
                            <Button type="submit" color="primary">Desar</Button>
                        </Col>
                    </Row>
                </Form>
            </>
        );
    }
}






export default TeamEdit;
